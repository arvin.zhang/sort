package net.zb.examination.sort;

import cn.hutool.core.collection.CollUtil;
import net.zb.examination.sort.vo.SortVo;

import java.util.List;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/17, 初始化版本
 * @version 1.0
 **/
public class SortMain {


	public static void main(String[] args){
		SortService sortService = new SortService();
		List<SortVo> oriList = sortService.init();
		System.out.println("==================排序前====================");
		printList(oriList);
		sortService.sort(oriList);
		System.out.println("==================排序后====================");
		printList(oriList);
	}



	private static void printList(List<SortVo> oriList){
		for (SortVo sortVo : oriList){
			recurrencePrint("", sortVo);
		}
	}


	private static void recurrencePrint(String blank, SortVo sortVo){
		System.out.println(blank + sortVo.getTitle());
		if(CollUtil.isNotEmpty(sortVo.getChildList())){
			for (SortVo s : sortVo.getChildList()){
				recurrencePrint(blank + "  ", s);
			}
		}
	}
}
