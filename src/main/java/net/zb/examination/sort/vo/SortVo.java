package net.zb.examination.sort.vo;

import java.util.List;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/17, 初始化版本
 * @version 1.0
 **/
public class SortVo {

	private String title;
	private List<SortVo> childList;



	public SortVo(){}


	public SortVo(String title){
		this.title = title;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<SortVo> getChildList() {
		return childList;
	}

	public void setChildList(List<SortVo> childList) {
		this.childList = childList;
	}
}
