package net.zb.examination.sort;

import cn.hutool.core.collection.CollUtil;
import net.zb.examination.sort.vo.SortVo;

import java.util.*;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/17, 初始化版本
 * @version 1.0
 **/
public class SortService {



	public List<SortVo> init(){
		List<SortVo> result = new ArrayList<>();
		SortVo nonmetals = new SortVo("Nonmetals");
		nonmetals.setChildList(CollUtil.newArrayList(new SortVo("Hydrogen"), new SortVo("Carbon"), new SortVo("Nitrogen"), new SortVo("Oxygen")));
		result.add(nonmetals);

		SortVo innerTrans = new SortVo("Inner Transitionals");
		SortVo lanthanides = new SortVo("Lanthanides");
		lanthanides.setChildList(CollUtil.newArrayList(new SortVo("Europium"), new SortVo("Cerium")));
		SortVo actinides = new SortVo("Actinides");
		actinides.setChildList(CollUtil.newArrayList(new SortVo("Uranium"), new SortVo("Plutonium"), new SortVo("Curium")));
		innerTrans.setChildList(CollUtil.newArrayList(lanthanides, actinides));
		result.add(innerTrans);

		SortVo alkali = new SortVo("Alkali Metals");
		alkali.setChildList(CollUtil.newArrayList(new SortVo("Lithium"), new SortVo("Sodium"), new SortVo("Potassium")));
		result.add(alkali);

		return result;
	}




	public List<SortVo> sort(List<SortVo> sortVoList){
		if(CollUtil.isEmpty(sortVoList)){
			return Collections.emptyList();
		}
		sortVoList.sort(Comparator.comparingInt(o -> o.getTitle().charAt(0)));
		for (SortVo sortVo : sortVoList){
			sort(sortVo.getChildList());
		}
		return sortVoList;
	}

}
